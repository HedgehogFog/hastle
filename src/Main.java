import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

import java.util.Scanner;

/**
 * Created by hedgehog on 16.08.17.
 */
public class Main{
    static Screen screen;
    static int countCloud = 0;
    static int countWait = 0;


    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Scanner scanner = new Scanner(System.in);
                if (scanner.nextLine().equals("exit"))
                    System.exit(1);
            }
        });

        screen = new Screen();

        thread.start();
        while (true){
            countCloud = 0;
            if (waitForClick())
                for (int i = 0; i < 6; i++)
                    clickCheck("img/cloud" + i + ".png");

            for (int i = 0; i < 1; i++)
                clickCheck("img/res" + i + ".png");

            for (int i = 0; i < 1; i++)
                clickTrobleCheck("img/troble" + i + ".png");

//            try {
//                screen.find("img/oops.png");
//                screen.click("img/free_space.png", 1);
//            } catch (Exception ex) {}
//
//            try {screen.dragDrop("img/free_space.png", "img/drag.png");} catch (Exception ex){}


            Thread.sleep(5000);
        }
    }

    private static boolean waitForClick() {
        for (int i = 0; i < 6; i++)
            findClouds("img/cloud" + i + ".png");

        if (countCloud >= 2) {
            countWait = 0;
            return true;
        }

        countWait++;

        if (countWait == 5)
            return true;

        return false;
    }

    private static void findClouds(String s) {
        try {
            screen.find(s);
            countCloud++;
        } catch (Exception ex){
        }
    }

    private static void clickCheck(String img){
        try {
            screen.click(img, 0);

        } catch (Exception ex){
        }
    }

    private static void clickTrobleCheck(String img){
        try {
            screen.click(img, 0);
            Thread.sleep(5000);
        } catch (Exception ex){
        }
    }
}

